local component = require("component")
local unicode = require("unicode")
local eeprom = component.eeprom

local config = ""

if not component.isAvailable("internet") then
    io.stderr:write("No internet card found.")
end

local function read()
    return io.read() or os.exit()
end

io.write("BIOS URL: ")
local url = read()
os.execute("wget -f " .. url .. " /tmp/dev-bios.bin")

local file = io.open("/tmp/dev-bios.bin", "r")
local data = file:read("*a")
file:close()

io.write("Flashing...")
local success, reason = eeprom.set(config .. data)

if not reason then
    eeprom.setLabel("Dev BIOS")
    eeprom.setData(require("computer").getBootAddress())
    
    io.write(" success.\n")
end